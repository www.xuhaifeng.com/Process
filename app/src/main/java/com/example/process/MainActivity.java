package com.example.process;

import android.app.ActivityManager;
import android.app.usage.UsageStats;
import android.app.usage.UsageStatsManager;
import android.content.Context;
import android.content.pm.ApplicationInfo;
import android.content.pm.PackageManager;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.util.Log;
import android.widget.ListView;

import java.util.ArrayList;
import java.util.List;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;

public class MainActivity extends AppCompatActivity {

    private static final String TAG = "MainActivity";

    private List<AppInfo> mInfoList = new ArrayList<>();
    private List<String> mStrings = new ArrayList<>();
    private ListView mListView;
    private AppInfoAdapter mAdapter;
    private boolean mAppInactive;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        mListView = findViewById(R.id.listView);
        mAdapter = new AppInfoAdapter(this, R.layout.item, mInfoList);
        mListView.setAdapter(mAdapter);


    }


    public void getTopActivityPackageName() {
        final UsageStatsManager usageStatsManager = (UsageStatsManager) this.getSystemService(Context.USAGE_STATS_SERVICE);
        if (usageStatsManager == null) {
            return;
        }
        mInfoList.clear();
        final PackageManager packageManager = this.getPackageManager();
        long time = System.currentTimeMillis();
        List<UsageStats> usageStatsList = usageStatsManager.queryUsageStats(UsageStatsManager.INTERVAL_BEST, time - 1000 * 60 * 60 * 3, time);
        if (usageStatsList != null) {
            for (UsageStats usageStats : usageStatsList) {
                String packageName = usageStats.getPackageName();
                try {


                    ApplicationInfo applicationInfo = packageManager.getApplicationInfo(packageName, 0);

                    Log.e(TAG, "package name = " + packageName);

                    String appName = applicationInfo.loadLabel(packageManager).toString();
                    Drawable drawable = applicationInfo.loadIcon(packageManager);
                    boolean b = usageStats.getTotalTimeInForeground() + usageStats.getLastTimeUsed() < System.currentTimeMillis();

                    Log.e(TAG, " appName = " + applicationInfo.loadLabel(packageManager).toString()
                            + "-" + usageStats.getTotalTimeInForeground() + "-" + usageStats.getLastTimeUsed()+"--"+b);
                    if ((applicationInfo.flags & ApplicationInfo.FLAG_SYSTEM) == 0) {
                        // 用户进程
                        if (!b) {
                            AppInfo info = new AppInfo();
                            info.setPckName(packageName);
                            info.setAppName(appName);
                            info.setDrawable(drawable);
                            mInfoList.add(info);
                        }

                    } else {
                        // 系统进程
                        AppInfo info = new AppInfo();
                        info.setPckName(packageName);
                        info.setAppName(appName);
                        info.setDrawable(drawable);
                        mInfoList.add(info);
                    }




                } catch (PackageManager.NameNotFoundException e) {
                    e.printStackTrace();
                }
            }
            mAdapter.notifyDataSetChanged();

        }

    }

    @Override
    protected void onResume() {
        super.onResume();
//        getAppInfos();
        boolean b = UsageUtils.checkAppUsagePermission(this);
        if (!b) {
            UsageUtils.requestAppUsagePermission(this);
            return;
        }
        getTopActivityPackageName();
    }

    private void getAppInfos() {
        final ActivityManager activityManager = (ActivityManager) this.getSystemService(Context.ACTIVITY_SERVICE);
        final PackageManager packageManager = this.getPackageManager();
        List<ActivityManager.RunningServiceInfo> runningTasks = activityManager.getRunningServices(Integer.MAX_VALUE);
        List<ActivityManager.RunningAppProcessInfo> runningAppProcesses = activityManager.getRunningAppProcesses();
        for (ActivityManager.RunningServiceInfo runningTaskInfo : runningTasks) {
            String packageName = runningTaskInfo.service.getPackageName();
            boolean foreground = runningTaskInfo.started;
            ApplicationInfo applicationInfo = null;
            try {
                applicationInfo = packageManager.getApplicationInfo(packageName, 0);

                if (foreground) {
                    String appName = applicationInfo.loadLabel(packageManager).toString();
                    Drawable drawable = applicationInfo.loadIcon(packageManager);
                    AppInfo info = new AppInfo();
                    info.setAppName(appName);
                    info.setPckName(packageName);
                    info.setDrawable(drawable);
                    if (!mStrings.contains(appName)) {
                        mStrings.add(appName);
                        mInfoList.add(info);
                    }
                    Log.e(TAG, "getAppInfos: packageName:  " + packageName
                            + "appName：" + appName + " icon：" + drawable);
                }
                if ((applicationInfo.flags & ApplicationInfo.FLAG_SYSTEM) == 0) {
                    // 用户进程


                } else {
                    // 系统进程
                }
            } catch (PackageManager.NameNotFoundException e) {
                e.printStackTrace();
            }

        }
        mAdapter.notifyDataSetChanged();

    }


}
