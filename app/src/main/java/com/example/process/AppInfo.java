package com.example.process;

import android.graphics.drawable.Drawable;

import java.util.Objects;

import androidx.annotation.Nullable;

/**
 * Created by xhf on 2020/3/25.
 */
public class AppInfo {
    private String pckName;
    private String appName;
    private Drawable mDrawable;

    public AppInfo() {
    }

    public String getPckName() {
        return pckName;
    }

    public void setPckName(String pckName) {
        this.pckName = pckName;
    }

    public String getAppName() {
        return appName;
    }

    public void setAppName(String appName) {
        this.appName = appName;
    }

    public Drawable getDrawable() {
        return mDrawable;
    }

    public void setDrawable(Drawable drawable) {
        mDrawable = drawable;
    }


}
