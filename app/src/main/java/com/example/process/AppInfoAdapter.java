package com.example.process;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.List;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

/**
 * Created by xhf on 2020/3/25.
 */
public class AppInfoAdapter extends ArrayAdapter<AppInfo> {

    private int redId;

    public AppInfoAdapter(@NonNull Context context, int resource, List<AppInfo> appInfos) {
        super(context, resource, appInfos);
        redId = resource;
    }

    @NonNull
    @Override
    public View getView(int position, @Nullable View convertView, @NonNull ViewGroup parent) {
        AppInfo info = getItem(position);
        View view;
        ViewHolder viewHolder;

        if (convertView == null){
            view = LayoutInflater.from(getContext()).inflate(redId, null);

            viewHolder = new ViewHolder();
            viewHolder.icon =  view.findViewById(R.id.img);
            viewHolder.appName =  view.findViewById(R.id.appName);
            viewHolder.pckName = view.findViewById(R.id.pckName);
            view.setTag(viewHolder);
        }else{
            view = convertView;
            viewHolder = (ViewHolder) view.getTag();
        }

        viewHolder.icon.setImageDrawable(info.getDrawable());
        viewHolder.appName.setText(info.getAppName());
        viewHolder.pckName.setText(info.getPckName());

        return view;
    }

    class ViewHolder{
        ImageView icon;
        TextView appName;
        TextView pckName;
    }
}
